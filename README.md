Dataset for Heitmann et al. (2017), Figure 6AB.
=====================

Variables in the Matlab file "Heitmann_et_al_PLoSCB_2017_Figure_6AB_LFP.mat":

 - array_map: 

10x10 integer array encoding the positions of the electrodes. 
Channels are indexed starting at 1, and 0 indicates missing or bad 
electrodes. One electrode (electrode 48) was excluded due to poor 
signal quality. The remaining absent ("0") electrodes were removed 
to make way for the optical fiber. Please see Heitmann et al. (2017) 
and Lu et al. (2015) for further details. 

 - gamma_lfp: 

60x95x1000 float64 array of gamma-LFP signals recorded during 
optogenetic stimulation. There are 60 trials, 95 recorded channels, 
and each trial last one second, sampled a 1 kilosamples / second. 
The spatial positions of each channel are identified in the 
`array_map` variable. Broad-band LFP has been band-pass filtered 
between 40 Hz-110 Hz and contains both low- and high-gamma LFP 
signals. Please see Heitmann et al. (2017) and Lu et al. (2015) for 
further details.

